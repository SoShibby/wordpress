FROM wordpress:5.2.4-php7.2-apache

RUN apt update && apt install -y git

# Add WP-CLI 
RUN curl -o /usr/local/bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN chmod +x /usr/local/bin/wp

# Add wp-graphql plugin (https://docs.wpgraphql.com)
RUN (cd /usr/src/wordpress/wp-content/plugins && git clone https://github.com/wp-graphql/wp-graphql.git)

